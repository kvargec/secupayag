<?php
use ApiTester;
class UtilCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    public function timeTest(ApiTester $I)
    {
        $I->amBearerAuthenticated('8067562d7138d72501485941246cf9b229c3a46a');
        $response = $I->sendGet('http://localhost/secu/util/time');
        $I->seeResponseCodeIs(200);
        $I->seeResponseContains('"result":"ok"');
        $I->seeResponseIsJson();
    }
}
