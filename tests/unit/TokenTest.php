<?php
require "config/config.php";

require "Msg.php";
class TokenTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testTokenValidation()
    {
        $access=new AccessModel();
        $good=$access->checkToken('8067562d7138d72501485941246cf9b229c3a46a');
        $bad=$access->checkToken('99f26159eb7a50784a9006fa35a5dbe32e604fee');
        $this->assertFalse($bad->code==200);
        $this->assertTrue($good->code==200);
    }
}