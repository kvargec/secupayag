<?php


class TransaktionTest extends \Codeception\Test\Unit
{

    public function testCanSee()
    {
        $test=new TransaktionModel();
        $good=$test->canSee(2,2);
        $bad=$test->canSee(3,2);
        $this->assertFalse($bad);
        $this->assertTrue($good);
    }
}
