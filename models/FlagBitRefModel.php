<?php

class FlagBitRefModel extends Database
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'stamd_flagbit_ref';
        $this->pk = 'flagbit_ref_id';
    }
    public function getByTransId($trans_id){
        $dtype=new DatensatzModel();
        $query="WHERE datensatz_typ_id=".$dtype->getByDesc('trans_id')." AND datensatz_id=".$trans_id." order by timestamp";
        return $this->findByQuery($query);
    }
}