<?php

class VorgabenZeitraumModel extends Database
{
    public function __construct()
    {
        parent::__construct();
        $this->table='vorgaben_zeitraum';
        $this->pk='zeitraum_id';
    }
}