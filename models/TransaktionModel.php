<?php

class TransaktionModel extends Database
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'transaktion_transaktionen';
        $this->pk = 'trans_id';
    }
    public function canSee($trans_id,$vertrag_id){
        $result=$this->findByPK($trans_id);
        if($result['vertrag_id']==$vertrag_id){
            return true;
        }else{
            return false;
        }
    }
}