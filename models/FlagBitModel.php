<?php

class FlagBitModel extends Database
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'vorgaben_flagbit';
        $this->pk = 'flagbit_id';
    }
}