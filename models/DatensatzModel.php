<?php

class DatensatzModel extends Database
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'vorgaben_datensatz_typ';
        $this->pk = 'datensatz_typ_id';
    }
    public function getByDesc($dec){
        $test=$this->findByAttr('beschreibung','trans_id');
        return $test[0]['datensatz_typ_id'];
    }
}