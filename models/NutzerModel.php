<?php

class NutzerModel extends Database
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'stamd_nutzerdetails';
        $this->pk = 'nutzerdetails_id';
    }
    public function getUserByToken($token){
        $access=new AccessModel();
        $vertrag_id=$access->getToken($token);
        return $vertrag_id['vertrag_id'];
    }
    public function isMaster($token){
        $access=new AccessModel();
        $test=$access->getToken($token);
        if($test['ist_masterkey']==1){
            return true;
        }else{
            return false;
        }
    }
    public function getUserId($vertrag){
        $test=new VertragsverwVertragModel();
        return $test->findByPK($vertrag);
    }
}