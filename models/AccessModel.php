<?php

class AccessModel extends Database
{
    public function __construct()
    {
        parent::__construct();
        $this->table='api_apikey';
        $this->pk='apikey_id';
    }
    public function getToken($token){
        $reza=$this->findByAttr('apikey',$token);
        return $reza[0];
    }
    public function checkToken($token){
        $token_id=$this->getToken($token);
        $response=[];
        if(!empty($token_id)){
            $zeitraum_id=$token_id['zeitraum_id'];
            $now=new DateTime();
            $zeitraum=new VorgabenZeitraumModel();
            $period=$zeitraum->findByPK($zeitraum_id);
            if(!empty($period)){
                $von=new DateTime($period['von']);
                $bis=new DateTime($period['bis']);
                if($von<$now && $now<$bis){
                    $msg=new Msg('ok',200,$token,'Token valid');
                }else{
                    $msg=new Msg('expired',403,'','Token expired');
                }
            }else{
                $msg=new Msg('error',403,'','Token not valid');
            }
        }else{
            $msg=new Msg('wrong',403,'','Wrong token');
            $response=['result'=>'wrong','code'=>403,'data'=>'','msg'=>'Wrong token'];
        }
        return $msg;
    }
}