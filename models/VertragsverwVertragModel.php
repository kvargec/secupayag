<?php

class VertragsverwVertragModel extends Database
{
    public function __construct()
    {
        parent::__construct();
        $this->table='vertragsverw_vertrag';
        $this->pk='vertrag_id';
    }
}