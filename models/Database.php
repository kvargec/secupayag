<?php
class Database
{
    protected $connection = null;
    protected $table='';
    protected $pk=null;
    public function __construct()
    {
        try {
            $this->connection = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE_NAME);
            if ( mysqli_connect_errno()) {
                throw new Exception("Could not connect to database.");
            }
            
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function findAll(){

        try {
            $stmt = $this->connection->prepare('SELECT * FROM '.$this->table);
            $stmt->execute();
            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
            $stmt->close();

            return $result;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        return false;
    }
    public function findByAttr($attr,$value){

        try {
            $pdo=$this->connection;
            $stmt = $pdo->prepare('SELECT * FROM '.$this->table.' where '.$attr.'=?');
            $stmt->execute([$value]);
            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
            $stmt->close();
            return $result;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        return false;
    }
    public function findByQuery($query){

        try {
            $pdo=$this->connection;
            $stmt = $pdo->prepare('SELECT * FROM '.$this->table.' '.$query);
            $stmt->execute();
            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
            $stmt->close();
            return $result;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        return false;
    }
    public function findByPK($value){

        try {
            $stmt = $this->connection->prepare('SELECT * FROM '.$this->table.' where '.$this->pk.'=?');
            $stmt->execute([$value]);
            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
            $stmt->close();
            if(!empty($result)){
                return $result[0];
            }else{
                return [];
            }

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        return false;
    }
    public function delete($value){

        try {
            $stmt = $this->connection->prepare('DELETE FROM '.$this->table.' where '.$this->pk.'=?');
            $stmt->execute([$value]);
            $stmt->close();


        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        return false;
    }
    public function insert($fields, $values){
        try {
            $forFields=implode(",",$fields);
            $forFields=$forFields;
            $forValues="?";
            for($i=1;$i<count($values);$i++){
                $forValues.=',?';
            }
            $ajde= 'INSERT INTO '.$this->table.' ('.$forFields.') VALUES ('.$forValues.')';

            $stmt = $this->connection->prepare('INSERT INTO '.$this->table.' ('.$forFields.') VALUES ('.$forValues.')');
            $stmt->execute($values);
            $result_id = $stmt->insert_id;
            $stmt->close();
            if($result_id){
                $stmt = $this->connection->prepare('SELECT * FROM '.$this->table.' where '.$this->pk.'=?');
                $stmt->execute([$result_id]);
                $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                $stmt->close();
            }

            if(!empty($result)){
                return $result[0];
            }else{
                return [];
            }

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        return false;
    }
    public function select($query = "", $params = [])
    {
        try {
            $stmt = $this->executeStatement($query, $params);
            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
            $stmt->close();

            return $result;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        return false;
    }

    private function executeStatement($query = "", $params = [])
    {
        try {

            $stmt = $this->connection->prepare($query);

            if ($stmt === false) {
                throw new Exception("Unable to do prepared statement: " . $query);
            }

            if ($params) {
                $stmt->bind_param($params[0], $params[1]);
            }

            $stmt->execute();

            return $stmt;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}