<?php
require __DIR__ . "/config/config.php";
require __DIR__ . "/Msg.php";

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );


switch ($uri[2]){
    case 'util':
        require PROJECT_ROOT_PATH . "/controllers/UtilController.php";
        $objFeedController = new UtilController();
        $strMethodName = $uri[3] . 'Action';
        $objFeedController->{$strMethodName}();
        break;
    case 'access':
        require PROJECT_ROOT_PATH . "/controllers/AccessController.php";
        $objFeedController = new AccessController();
        $strMethodName = $uri[3] . 'Action';
        $objFeedController->{$strMethodName}();
        break;
    case 'flag-bits':
        require PROJECT_ROOT_PATH . "/controllers/FlagBitsController.php";
        $objFeedController = new FlagBitsController();
        $strMethodName = $uri[3] . 'Action';
        $objFeedController->{$strMethodName}();
        break;
}
