Some notes for task:

All models are in models folder, I tried to use some general Database class for that.
For API testing I added only for time step, can do for more (you need to change url in that file for testing).
I added simple unit testing
In index.php file is simple router, can be done better. All callable endpoints have Action suffix in controllers
I create class for returning messages to user, it can be redifined

Usage:
URL is main server URL
URL/util/time - for server time
URL/access/check_token - checking is access token valid
URL/flag-bits/list?trans_id - Active flagbits for transaction
URL/flag-bits/enable - enable flagbit for transaction example of payload data { "datensatz_typ_id": 2, "datensatz_id": 2,"flagbit": 4,"zeitraum_id": 2}
URL/flag-bits/disable - disable flagbit for transaction example of payload data { "datensatz_typ_id": 2, "datensatz_id": 2,"flagbit": 4}
URL/flag-bits/history?trans_id - history of flagbits for some transaction

