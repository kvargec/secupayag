<?php
const PROJECT_ROOT_PATH = __DIR__ . "/../";

// include main configuration file
require_once PROJECT_ROOT_PATH . "/config/db.php";
require_once PROJECT_ROOT_PATH . "/config/const.php";
// include the base controller file
require_once PROJECT_ROOT_PATH . "/controllers/BaseController.php";

// include the use model file
require_once PROJECT_ROOT_PATH . "/models/Database.php";

require_once PROJECT_ROOT_PATH . "/models/VorgabenZeitraumModel.php";
require_once PROJECT_ROOT_PATH . "/models/AccessModel.php";
require_once PROJECT_ROOT_PATH . "/models/NutzerModel.php";
require_once PROJECT_ROOT_PATH . "/models/FlagBitModel.php";
require_once PROJECT_ROOT_PATH . "/models/FlagBitRefModel.php";
require_once PROJECT_ROOT_PATH . "/models/DatensatzModel.php";
require_once PROJECT_ROOT_PATH . "/models/TransaktionModel.php";
require_once PROJECT_ROOT_PATH . "/models/VertragsverwVertragModel.php";

?>

