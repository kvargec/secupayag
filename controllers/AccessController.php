<?php


class AccessController extends BaseController
{
    public function check_tokenAction(){
            $strErrorDesc = '';
            $forHeader=array('Content-Type: application/json', 'HTTP/1.1 200 OK');
            try {
                $tokenValidation=$this->tokenValidation();
                $msg=$tokenValidation;
                switch ($tokenValidation->code){
                    case 200:
                        $forHeader=array('Content-Type: application/json', 'HTTP/1.1 200 OK');
                        break;
                    case 403:
                        $forHeader=array('Content-Type: application/json', 'HTTP/1.1 403 Forbbiden');
                        break;
                }
            } catch (Error $e) {
                $strErrorDesc = $e->getMessage();
                $msg=new Msg('error',500,[],$strErrorDesc);

                $forHeader=array('Content-Type: application/json', 'HTTP/1.1 500 Internal Server Error');
            }
            $responseData=$msg->getResponse(true);
            $this->sendOutput(
                $responseData,
                $forHeader
            );
    }
    public function tokenValidation(){
        $model=new AccessModel();
        $tokenValidation=$model->checkToken($this->getBearerToken());
        return $tokenValidation;
    }
    public function getAuthorizationHeader(){
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }
    /**
     * get access token from header
     * */
    public function getBearerToken() {
        $headers = $this->getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }
}