<?php

include 'AccessController.php';

class UtilController extends BaseController
{

    public function timeAction()
    {
        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];

        if (strtoupper($requestMethod) == 'GET') {
            try {
                $access=new AccessController();
                $token=$access->tokenValidation();
                if($token->code==200){
                    $server_time=date('Y-m-d H:i:s',time());
                    $msg=new Msg('ok',200,['server_time'=>$server_time],'');

                }else{
                    $response=$token;
                }
                switch ($token->code){
                    case 200:
                        $forHeader=array('Content-Type: application/json', 'HTTP/1.1 200 OK');
                        break;
                    case 403:
                        $forHeader=array('Content-Type: application/json', 'HTTP/1.1 403 Forbbiden');
                        break;
                }
            } catch (Error $e) {
                $strErrorDesc = $e->getMessage();
                $msg=new Msg('error',500,[],$strErrorDesc);

                $forHeader=array('Content-Type: application/json', 'HTTP/1.1 500 Internal Server Error');
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $msg=new Msg('error',442,[],$strErrorDesc);

            $forHeader=array('Content-Type: application/json', 'HTTP/1.1 422 Unprocessable Entity');
        }

        $responseData=$msg->getResponse(true);
        $this->sendOutput(
            $responseData,
            $forHeader
        );
    }
}