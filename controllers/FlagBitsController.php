<?php
include 'AccessController.php';

class FlagBitsController extends BaseController
{
    public function listAction()
    {
        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];

        if (strtoupper($requestMethod) == 'GET') {
            try {
                $arrQueryStringParams = $this->getQueryStringParams();
                $trans_id = $arrQueryStringParams['trans_id'];
                $access = new AccessController();
                $token = $access->tokenValidation();
                if ($token->code == 200) {
                    $results = [];
                    $getUser = new NutzerModel();
                    $vertrag = $getUser->getUserByToken($token->data);
                    $trans = new TransaktionModel();
                    $fb = new FlagBitModel();
                    $flagBitsAll = array_column($fb->findAll(), 'flagbit_id');
                    if ($trans->canSee($trans_id, $vertrag)) {
                        $flagBits = new FlagBitRefModel();
                        $zeitraum = new VorgabenZeitraumModel();
                        $now = new DateTime();
                        $getTrans = $flagBits->getByTransId($trans_id);
                        foreach ($getTrans as $item) {
                            $period = $zeitraum->findByPK($item['zeitraum_id']);
                            $von = new DateTime($period['von']);
                            $bis = new DateTime($period['bis']);
                            if ($von < $now && $now < $bis && in_array($item['flagbit'], $flagBitsAll)) {
                                $temp = new ReflectionClass("DataFlag");
                                $forNames = array_flip($temp->getConstants());
                                $item['flagbit_name'] = $forNames[$item['flagbit']];
                                $results[] = $item;
                            }
                        }
                        $msg = new Msg('ok', 200, $results, 'List of active flagbits');
                    } else {
                        $msg = new Msg('ok', 200, $results, 'No entries for that trans_id ');
                    }
                } else {
                    $msg = $token;
                }
                switch ($token->code) {
                    case 200:
                        $forHeader = array('Content-Type: application/json', 'HTTP/1.1 200 OK');
                        break;
                    case 403:
                        $forHeader = array('Content-Type: application/json', 'HTTP/1.1 403 Forbbiden');
                        break;
                }

            } catch (Error $e) {
                $strErrorDesc = $e->getMessage();
                $msg = new Msg('error', 500, [], $strErrorDesc);

                $forHeader = 'HTTP/1.1 500 Internal Server Error';
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $msg = new Msg('error', 442, [], $strErrorDesc);

            $forHeader = array('Content-Type: application/json', 'HTTP/1.1 422 Unprocessable Entity');
        }
        $responseData = $msg->getResponse(true);
        $this->sendOutput($responseData, $forHeader);
    }
    public function enableAction()
    {
        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];

        if (strtoupper($requestMethod) == 'POST') {
            try {
                $input = (array) json_decode(file_get_contents('php://input'), TRUE);
                $access = new AccessController();
                $token = $access->tokenValidation();
                if ($token->code == 200) {
                    $results = [];
                    $trans_type_id = $input['datensatz_typ_id'];
                    $trans_id = $input['datensatz_id'];
                    $flagbit = $input['flagbit'];
                    $zeitraum_id = $input['zeitraum_id'];

                    $getUser = new NutzerModel();
                    $vertrag = $getUser->getUserByToken($token->data);
                    $user=$getUser->getUserId($vertrag);
                    $trans = new TransaktionModel();
                    $fb = new FlagBitModel();
                    $flagBitsAll = array_column($fb->findAll(), 'flagbit_id');
                    $forInsert=new FlagBitRefModel();
                    if(in_array($flagbit, $flagBitsAll)){
                        $test=$forInsert->insert(['datensatz_typ_id','datensatz_id','flagbit','zeitraum_id','bearbeiter_id'],[$trans_type_id,$trans_id,$flagbit,$zeitraum_id,$user['nutzer_id']]);
                        if(!empty($test)){
                            $results[]=$test;
                            $msg = new Msg('ok', 201, $results, 'Created');
                        }else{
                            $msg = new Msg('error', 200, $results, 'Error creating');
                        }

                    } else {
                        $msg = new Msg('error', 200, $results, 'Wrong flagbit ');
                    }

                } else {
                    $msg = $token;
                }
                switch ($token->code) {
                    case 200:
                        $forHeader = array('Content-Type: application/json', 'HTTP/1.1 200 OK');
                        break;
                    case 403:
                        $forHeader = array('Content-Type: application/json', 'HTTP/1.1 403 Forbbiden');
                        break;
                }
                if($msg->code==201){
                    $forHeader = array('Content-Type: application/json', 'HTTP/1.1 201 CREATED');
                }

            } catch (Error $e) {
                $strErrorDesc = $e->getMessage();
                $msg = new Msg('error', 500, [], $strErrorDesc);

                $forHeader = 'HTTP/1.1 500 Internal Server Error';
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $msg = new Msg('error', 442, [], $strErrorDesc);

            $forHeader = array('Content-Type: application/json', 'HTTP/1.1 422 Unprocessable Entity');
        }
        $responseData = $msg->getResponse(true);
        $this->sendOutput($responseData, $forHeader);
    }
    public function disableAction()
    {
        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];

        if (strtoupper($requestMethod) == 'DELETE') {
            try {
                $input = (array) json_decode(file_get_contents('php://input'), TRUE);
                $access = new AccessController();
                $token = $access->tokenValidation();
                if ($token->code == 200) {
                    $results = [];
                    $trans_type_id = $input['datensatz_typ_id'];
                    $trans_id = $input['datensatz_id'];
                    $flagbit = $input['flagbit'];
                    $temp=new FlagBitRefModel();
                    $whatToDelete=$temp->findByQuery('WHERE datensatz_typ_id='.$trans_type_id.' AND datensatz_id='.$trans_id.' AND flagbit='.$flagbit);
                    $getUser = new NutzerModel();
                    $fb = new FlagBitModel();
                    $flagBitsAll = array_column($fb->findAll(), 'flagbit_id');
                    if($getUser->isMaster($token->data)){
                        $forDelete=new FlagBitRefModel();
                        $del_id=$forDelete->findByPK($whatToDelete[0]['flagbit_ref_id']);
                        $test=$forDelete->delete($del_id['flagbit_ref_id']);
                        $msg = new Msg('ok', 200, $results, 'Deleted');
                    }else{
                        $msg = new Msg('forbbiden', 403, [], 'Forbbiden');
                    }
                } else {
                    $msg = $token;
                }
                switch ($token->code) {
                    case 200:
                        $forHeader = array('Content-Type: application/json', 'HTTP/1.1 200 OK');
                        break;
                    case 403:
                        $forHeader = array('Content-Type: application/json', 'HTTP/1.1 403 Forbbiden');
                        break;
                }
                if($msg->code==201){
                    $forHeader = array('Content-Type: application/json', 'HTTP/1.1 201 CREATED');
                }

            } catch (Error $e) {
                $strErrorDesc = $e->getMessage();
                $msg = new Msg('error', 500, [], $strErrorDesc);

                $forHeader = 'HTTP/1.1 500 Internal Server Error';
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $msg = new Msg('error', 442, [], $strErrorDesc);

            $forHeader = array('Content-Type: application/json', 'HTTP/1.1 422 Unprocessable Entity');
        }
        $responseData = $msg->getResponse(true);
        $this->sendOutput($responseData, $forHeader);
    }
    public function historyAction()
    {
        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];

        if (strtoupper($requestMethod) == 'GET') {
            try {
                $arrQueryStringParams = $this->getQueryStringParams();
                $trans_id = $arrQueryStringParams['trans_id'];
                $access = new AccessController();
                $token = $access->tokenValidation();
                if ($token->code == 200) {
                    $results = [];
                    $getUser = new NutzerModel();
                    $vertrag = $getUser->getUserByToken($token->data);
                    $trans = new TransaktionModel();
                    $fb = new FlagBitModel();
                    $flagBitsAll = array_column($fb->findAll(), 'flagbit_id');
                    if ($trans->canSee($trans_id, $vertrag)) {
                        $flagBits = new FlagBitRefModel();
                        $getTrans = $flagBits->getByTransId($trans_id);
                        foreach ($getTrans as $item) {

                            if ( in_array($item['flagbit'], $flagBitsAll)) {
                                $temp = new ReflectionClass("DataFlag");
                                $forNames = array_flip($temp->getConstants());
                                $item['flagbit_name'] = $forNames[$item['flagbit']];
                                $results[] = $item;
                            }
                        }
                        $msg = new Msg('ok', 200, $results, 'List of history flagbits');
                    } else {
                        $msg = new Msg('ok', 200, $results, 'No entries for that trans_id ');
                    }
                } else {
                    $response = $token;
                }
                switch ($token->code) {
                    case 200:
                        $forHeader = array('Content-Type: application/json', 'HTTP/1.1 200 OK');
                        break;
                    case 403:
                        $forHeader = array('Content-Type: application/json', 'HTTP/1.1 403 Forbbiden');
                        break;
                }

            } catch (Error $e) {
                $strErrorDesc = $e->getMessage();
                $msg = new Msg('error', 500, [], $strErrorDesc);

                $forHeader = 'HTTP/1.1 500 Internal Server Error';
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $msg = new Msg('error', 442, [], $strErrorDesc);

            $forHeader = array('Content-Type: application/json', 'HTTP/1.1 422 Unprocessable Entity');
        }
        $responseData = $msg->getResponse(true);
        $this->sendOutput($responseData, $forHeader);
    }
}
