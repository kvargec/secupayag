<?php

class Msg
{
    public $result='';
    public $code='';
    public $data='';
    public $msg='';

    /**
     * @param string $result
     * @param string $code
     * @param string $msg
     */
    public function __construct(string $result, string $code, $data, string $msg)
    {
        $this->result = $result;
        $this->code = $code;
        $this->data = $data;
        $this->msg = $msg;
    }
    public function getResponse($encoded=false){
        $response=['result'=>$this->result,'code'=>$this->code,'data'=>$this->data,'msg'=>$this->msg];
        if($encoded){
            $response=json_encode($response);
        }
        return $response;
    }
    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * @param string $result
     */
    public function setResult(string $result): void
    {
        $this->result = $result;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        return $this->data;
    }

    /**
     * @param string $data
     */
    public function setData(string $data): void
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getMsg(): string
    {
        return $this->msg;
    }

    /**
     * @param string $msg
     */
    public function setMsg(string $msg): void
    {
        $this->msg = $msg;
    }

}